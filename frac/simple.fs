#version 400 core

out vec4 color;

vec4 white = vec4(1, 1, 1, 1);
vec4 black = vec4(0, 0, 0, 1);
void main(){
	float cx = 1.92*(2*(gl_FragCoord.x/1920) - 1);
	float cy = 1.08*(2*(gl_FragCoord.y/1080) - 1);
	float zx = 0;
	float zy = 0;
	for (uint i = 0; i < 1000; i++){
		float zx_n = zx*zx - zy*zy + cx;
		zy = 2*zx*zy + cy;
		zx = zx_n;
	}
	if ((zx*zx + zy*zy > 0)){
		color = white;
	} else {
		color = black;
	}
}
