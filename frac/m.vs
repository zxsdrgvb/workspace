#version 400 core

void main(){
	vec4 vertices[4] = vec4[4](
		vec4(-1,	-1,	1,	1),
		vec4(+1,	-1,	1,	1),
		vec4(-1,	+1,	1,	1),
		vec4(+1,	+1,	1,	1)
	);
	gl_Position = vertices[gl_VertexID];
}
