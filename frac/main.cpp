#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>
//#include <glm/gtx/transform.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.cpp"

uint max = 100;
float zx0 = 0;
float zy0 = 0;
float boundary[4] = {-1.92, -1.08, 1.92, 1.08};


GLuint vao = -1;
void before_render_loop(){
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
}

void render(GLFWwindow* window, GLuint program, double current_time){
	GLfloat bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	GLint iboundary = glGetUniformLocation(program, "boundary");
	glUniform4fv(iboundary, 1, boundary);

	GLint imax = glGetUniformLocation(program, "max");
	glUniform1ui(imax, max);

	GLint izx0 = glGetUniformLocation(program, "zx0");
	glUniform1f(izx0, zx0);

	GLint izy0 = glGetUniformLocation(program, "zy0");
	glUniform1f(izy0, zy0);
//	glClearBufferfv(GL_COLOR, 0, bgcolor);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void after_render_loop(){

}

void cursorpos_callback(GLFWwindow *window, double xpos, double ypos){
}
//we'll put a stopper on the scroller???
/*
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset){
	zx0 = (float) yoffset/1000;
}
*/
//I think I"ll make it so that it doesn't set the callback if its a null pointer
//if I can auto intiailize the function poiners to null then im golden

uint key_inc = 10;
float key_step0 = 0.01;
void key_update(char* key_array){
	if (key_array[GLFW_KEY_R]) {
		float scale = 3;
		boundary[0] = -1.92*scale;
		boundary[1] = -1.08*scale;
		boundary[2] = +1.92*scale;
		boundary[3] = +1.08*scale;
	}
//	printf("key_step0 - %f\n", key_step0);
	if (key_array[GLFW_KEY_1]){		key_inc = 10;}
	if (key_array[GLFW_KEY_2]){		key_inc = 1;}
	if (key_array[GLFW_KEY_3]){		key_inc = 100;}
	if (key_array[GLFW_KEY_0]){		max = key_inc;}
	if (key_array[GLFW_KEY_7]){		key_step0 = 0.01;}
	if (key_array[GLFW_KEY_8]){		key_step0 = 0.001;}
	if (key_array[GLFW_KEY_9]){		key_step0 = 0.000001;}
	if (key_array[GLFW_KEY_UP]){		zx0 += key_step0;  printf("zx0, zy0 - %f, %f\n", zx0, zy0);}
	if (key_array[GLFW_KEY_DOWN]){		zx0 -= key_step0;}
	if (key_array[GLFW_KEY_RIGHT]){		zy0 += key_step0;}
	if (key_array[GLFW_KEY_LEFT]){		zy0 -= key_step0;}
	if (key_array[GLFW_KEY_X]){		max += key_inc;}
	if (key_array[GLFW_KEY_Z]){
		max -= key_inc;
		if (max <= 0) max = key_inc; //in assembly you compare max with 10 by subtracting them so this is efficiently the same
	}

}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		printf("xpos, ypos - %f, %f\n", xpos, ypos);
		float xpos_norm = ((float) xpos)/1920;
		float ypos_norm = ((float) ypos)/1080;
		printf("xpos_norm, \" - %f, %f\n", xpos_norm, ypos_norm);
		float xn1 = (xpos_norm - 0.10) * (boundary[2] - boundary[0]) + boundary[0];
		float yn1 = (ypos_norm - 0.10)* (boundary[3] - boundary[1]) + boundary[1];
		float xn2 = (xpos_norm + 0.10) * (boundary[2] - boundary[0]) + boundary[0];
		float yn2 = (ypos_norm + 0.10)* (boundary[3] - boundary[1]) + boundary[1];
		printf("xn, \" - %f, %f, %f, %f\n", xn1, yn1, xn2, yn2);
		boundary[0] = xn1;
		boundary[1] = yn1;
		boundary[2] = xn2;
		boundary[3] = yn2;
		return;
	}
}

int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "m.vs";
	my_app.fragment_source_path 	= "m.fs";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_update		= &key_update;
	my_app.cursorpos_callback	= &cursorpos_callback;
	my_app.mouse_button_callback	= &mouse_button_callback;

	run(my_app);
}
