#version 400 core

out vec4 color;

vec4 white = vec4(1, 1, 1, 1);
vec4 black = vec4(0, 0, 0, 1);

uniform vec4 boundary;
uniform uint max;
uniform float zx0;
uniform float zy0;

float scale = 2;
void main(){
	float cx = (gl_FragCoord.x/1920)*(boundary.z - boundary.x) + boundary.x;
	float cy = (gl_FragCoord.y/1080)*(boundary.y - boundary.w) + boundary.w;
	float zx = zx0;
	float zy = zy0;
	uint i = 0;
	while (((zx*zx + zy*zy) < 4) && (i < max)){
		float zx_n = zx*zx - zy*zy + cx;
		zy = 2*zx*zy + cy;
		zx = zx_n;
		zx = zx;
		zy = -zy; //glsl will probably optimise this i dont need to worry about that
		i++;
	}
	color = vec4(float(i) / float(max) * vec3(1, 1, 1), 1);
}
