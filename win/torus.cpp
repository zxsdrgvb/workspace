#include <math.h>
#include <stdio.h>

//storage_pointer should be to an array with size at least 3 * theta_divisions * phi_divisions float's
void torus_rings(float* storage_pointer, size_t theta_divisions, size_t phi_divisions, float small_r, float big_r){
	float delta_theta = 2*M_PI / theta_divisions;
	float delta_phi = 2*M_PI / phi_divisions;
	float theta = 0;
	float phi = 0;
	float cos_theta = 1;
	float sin_theta = 0;
	for (size_t i = 0; i < theta_divisions * phi_divisions; i++){
		if (i % phi_divisions == 0) {
			phi = 0;
//			phi = (float) i * .00001; //this gives it a swirl
			theta += delta_theta;
			cos_theta = cos(theta);
			sin_theta = sin(theta);
		}
		storage_pointer[3*i] 		= (big_r + small_r * cos(phi)) * cos_theta;
		storage_pointer[3*i + 1] 	= (big_r + small_r * cos(phi)) * sin_theta;
		storage_pointer[3*i + 2] 	= small_r * sin(phi);

		phi += delta_phi;
//		printf("theta: %f\nphi: %f\n2pi: %f\n", theta, phi, 2*M_PI);
	}
	printf("Torus Loaded.");
}
//size of buffer should be 2*(1 + phi_divisions)*theta_divisisions

void torus_get_triangle_indices(uint* storage_pointer, size_t theta_divisions, size_t phi_divisions){
	//primitive restart index = 0xFFFFFFFF
	uint i = 0;
	for (uint theta_no = 0; theta_no < theta_divisions; theta_no++){
		uint theta_next = theta_no + 1;
		if (theta_next == theta_divisions) theta_next = 0;
		for (uint phi_no = 0; phi_no < phi_divisions; phi_no++){
			storage_pointer[i++] = theta_no * phi_divisions + phi_no;
			storage_pointer[i++] = theta_next * phi_divisions + phi_no;
		}
		storage_pointer[i++] = theta_no * phi_divisions;
		storage_pointer[i++] = theta_next * phi_divisions;
		storage_pointer[i++] = 0xFFFFFFFF;
	}
	printf("HERE IS I - %d\n", i);


}












/*
void torus_get_triangle_indices(uint* storage_pointer, size_t theta_divisions, size_t phi_divisions){
	//primitive restart index = 0xFFFFFFFF
//	tdiv * (pdiv * 2 + 1)
	uint i = 0;
	uint theta_no, phi_no;
	for (theta_no = 0; theta_no < theta_divisions - 1; theta_no++){
		for (phi_no = 0; phi_no < phi_divisions - 1; phi_no++){
			storage_pointer[i++] = theta_no*phi_divisions + phi_no;
			storage_pointer[i++] = (theta_no + 1)*phi_divisions + phi_no;
		}
		storage_pointer[i++] = theta_no*phi_divisions;
		storage_pointer[i++] = (theta_no + 1)*phi_divisions;
		storage_pointer[i++] = 0xFFFFFFFF;
	}
	for (phi_no = 0; phi_no < phi_divisions - 1; phi_no++){
		storage_pointer[i++] = theta_no*phi_divisions + phi_no;
		storage_pointer[i++] = phi_no;
	}
	storage_pointer[i++] = theta_no * phi_divisions + phi_no;
	storage_pointer[i++] = 0;
	storage_pointer[i++] = 0xFFFFFFFF;
	printf("size is %d\n", i);
}
*/
/*
int main(){
	size_t tdev = 100;
	size_t pdev = 20;
	float* store_ptr = (float*) malloc(3*tdev*pdev*sizeof(float));
	if (store_ptr == NULL){
		printf("Malloc Failed\n");
		exit(1);
	}
	torus_rings(store_ptr, tdev, pdev, 10, 1);
	printf("Success?\n");

	for (size_t i = 0; i < tdev*pdev; i++){
		printf("%10.3f %10.3f %10.3f\n",store_ptr[3*i],
					store_ptr[3*i + 1],
					store_ptr[3*i + 2]);
	}

	return 0;
}
*/
