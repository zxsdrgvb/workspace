#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>

static void my_key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

int main(){
	printf("test");

	if (!glfwInit()){
		printf("Fail!");
		return 1;
	}
	GLFWwindow *window = glfwCreateWindow(640, 480, "Event Tester", NULL, NULL);
	if (!window){
		fprintf(stderr, "GLFW failed to create window.\n");
	}
	glfwMakeContextCurrent(window);
	glewInit();
	printf("GLEW has initialized.\n");

	glfwSetKeyCallback(window, my_key_callback);
	while (!glfwWindowShouldClose(window)){
//		printf("Count: %d\n", ++i);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}

static void my_key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}
