#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <cmath>

#include "src/application.h"

class MyProgram : public Application {
public:
	GLfloat red[4]	 = {1.0f, 0.0f, 0.0f, 1.0f};
	GLfloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat gray[4]	 = {0.3f, 0.3f, 0.3f, 1.0f};

	void render(double current_time) override{
		glClearBufferfv(GL_COLOR, 0, gray);
		glUseProgram(this->program);
		glDrawArrays(GL_POINTS, 0, 1);
	}
};

int main(){
	MyProgram program;
	program.add_shaders("shaders/vertex.shader", "shaders/fragment.shader");
	program.run();
}
