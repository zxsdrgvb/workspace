#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>

class Application {
public:
	int window_width 	= 0;
	int window_height 	= 0;
	const char* window_title	= NULL;

	const char* vertex_source;
	const char* fragment_source;


	Application(int width = 1920, int height = 1080, const char* title = "Event Tester");
	void add_shaders_directly(const char* vertex_shader_source, const char* fragment_shader_source);
	void add_shaders(const char* vertex_source_path, const char* fragment_source_path);
	void compile_shaders();
//	~Application() {};
	int run();
	static void error_callback(int error, const char* description);

	virtual void render(double current_time) = 0;
	virtual void before_render_loop() {};
	virtual void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
protected:
	GLFWwindow *window = NULL;

	GLuint vertex_shader	= 0;
	GLuint fragment_shader 	= 0;
	GLuint program 		= 0;


	int init_window();
};

