#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include "application.h"
#include <string>

void Application::add_shaders(const char* vertex_source_path, const char* fragment_source_path){
	char buffer[1000];
	FILE* vss_fptr = fopen(vertex_source_path, "r");
	if (!vss_fptr) printf("VERTEX SHADER IS NULL??\n");
	size_t chars_read = fread(buffer, sizeof(char), 1000, vss_fptr);
	if (chars_read >= 1000) {
		printf("Buffer too small");
	}
	buffer[chars_read] = '\0';
	const char* vss = buffer;

	char buffer2[1000];
	FILE* fss_fptr = fopen(fragment_source_path, "r");
	size_t chars_read2 = fread(buffer2, sizeof(char), 1000, fss_fptr);
	if (chars_read2 >= 1000) {
		printf("Buffer too small");
	}
	buffer2[chars_read2] = '\0';
	const char* fss = buffer2;

	this->add_shaders_directly(vss, fss);
	printf("\nVERTEX SOURCE:\n%s\n", vss);
	printf("\nFRAGMENT SOURCE:\n%s\n", fss);
}

void Application::add_shaders_directly(const char* vertex_shader_source, const char* fragment_shader_source){
	this->vertex_source = vertex_shader_source;
	this->fragment_source = fragment_shader_source;
}

GLuint compile_single_shader(const char** source_ptr, GLenum shader_type){
	GLuint shader = glCreateShader(GL_VERTEX_SHADER);
	if (shader <= 0) {
		fprintf(stderr, "GL failed to create shader.");
		return -1;
	}
	glShaderSource(shader, 1, source_ptr, NULL);
	glCompileShader(shader);

	GLint is_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);

	GLint log_length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);

	std::string str;
	str.reserve(log_length);
	glGetShaderInfoLog(shader, log_length, NULL, (GLchar *) str.c_str());
	printf("SHADER COMPILATION LOG:\n%s\n", str.c_str());

	if (!is_compiled){
		printf("\nSHADER COMPILATION FAILED!!!\n");
	}
	return shader;
}

void Application::compile_shaders(){
	this->vertex_shader   = compile_single_shader(&this->vertex_source, GL_VERTEX_SHADER);
	this->fragment_shader = compile_single_shader(&this->fragment_source, GL_FRAGMENT_SHADER);

	this->program = glCreateProgram();
	glAttachShader(this->program, this->vertex_shader);
	glAttachShader(this->program, this->fragment_shader);
	glLinkProgram(this->program);
}

Application::Application(int width, int height, const char* title){
	this->window_width = width;
	this->window_height = height;
	this->window_title = title;
}

void Application::error_callback(int error, const char* description){
	fprintf(stderr, "Error %d: %s\n", error, description);
}

int Application::run() {
	if (this->init_window() != 0) {
		fprintf(stderr, "\nWindow initialization has failed.\n");
		return 1;
	}
	glewInit();
	glfwSetErrorCallback(error_callback);

	glfwSetWindowUserPointer(this->window, this);
	glfwSetKeyCallback(this->window,
			[](GLFWwindow *window, int key, int scancode, int action, int mods) -> void {
				Application *app = (Application*) glfwGetWindowUserPointer(window);
				app->key_callback(window, key, scancode, action, mods);
			});

	printf("Current GL version: %s\n", glGetString(GL_VERSION));
	printf("Current GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	this->compile_shaders();

	this->before_render_loop();
	while (!glfwWindowShouldClose(this->window)){
		this->render(glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}


	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}

int Application::init_window(){
	if (!glfwInit()){
		fprintf(stderr, "GLFW failed to initialize.");
		return 1;
	}

	this->window = glfwCreateWindow(this->window_width, this->window_height, this->window_title, NULL, NULL);
	if (!this->window){
		fprintf(stderr, "GLFW failed to create window.\n");
		return 1;
	}
	glfwMakeContextCurrent(window);
	return 0;
}

void Application::key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}
