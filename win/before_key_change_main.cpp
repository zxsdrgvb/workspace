#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>
//#include <glm/gtx/transform.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.cpp"
#include "camera.cpp"
#include "torus.cpp"

size_t theta_divisions = 20;
size_t phi_divisions = 10;
float small_r = 1;
float big_r = 2;
//size_t IBO_SIZE = (2*(theta_divisions+1)*(phi_divisions + 1))*sizeof(uint);
int supposed_i = 2 * (theta_divisions) * (phi_divisions + 1) + theta_divisions ;
size_t IBO_SIZE = supposed_i * sizeof(uint);
bool POLY_FLAG = false;

GLuint vertex_array_object = -1;
GLuint torus_buffer = -1;
GLuint index_buffer = -1;
Camera camera;

void before_render_loop(){
	printf("Poly Flag? ");
//	char x;
//	scanf("%c", &x);
//	POLY_FLAG = (x == 'y');
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	glGenBuffers(1, &torus_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, torus_buffer);
	glBufferData(GL_ARRAY_BUFFER, 3*theta_divisions*phi_divisions*sizeof(float) , NULL, GL_STATIC_DRAW);
	float* torus_buffer_ptr = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	torus_rings(torus_buffer_ptr, theta_divisions, phi_divisions, small_r, big_r);
	glUnmapBuffer(GL_ARRAY_BUFFER);

	glGenBuffers(1, &index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, IBO_SIZE, NULL, GL_STATIC_DRAW);
	uint* index_buffer_ptr = (uint*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
	torus_get_triangle_indices(index_buffer_ptr, theta_divisions, phi_divisions);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	camera.eye 	= {0.0f, 0.0f, -10.0f};
	camera.forward 	= {0.0f, 0.0f, 1.0f};
	printf("IBO SIZE - %d\nSupposed I - %d\n", IBO_SIZE, supposed_i);
}

void render(GLFWwindow* window, GLuint program, double current_time){
	GLfloat bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glm::mat4 P = glm::perspectiveFov(120.0f, (float) width, (float) height, 0.01f, 100.0f);
	glViewport(0, 0, (float) width, (float) height);
	glm::mat4 V = view_matrix(camera);
	glm::mat4 M = glm::mat4(1.0f);
	M = glm::rotate(M, (float) current_time, glm::vec3(0.0f, 0.0f, 1.0f));
	//M = glm::rotate(M, (float) log(current_time), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 MVP = P * V * M;

	GLint iMVP = glGetUniformLocation(program, "MVP");
	glUniformMatrix4fv(iMVP, 1, false, glm::value_ptr(MVP));

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (POLY_FLAG) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(0xFFFFFFFF);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDrawElements(GL_TRIANGLE_STRIP, IBO_SIZE, GL_UNSIGNED_INT, 0);
//	glDrawArrays(GL_POINTS, 0, theta_divisions*phi_divisions);

}

void after_render_loop(){
	glDeleteVertexArrays(1, &vertex_array_object);
}

void cursorpos_callback(GLFWwindow *window, double xpos, double ypos){
	float angle_inc = 0.0005;
	look_up(camera, -angle_inc*ypos);
	look_left(camera, -angle_inc*xpos);
	glfwSetCursorPos(window, 0, 0);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	float angle_inc = 0.05;
	float distance_inc = 0.8;
	switch(key){
		case GLFW_KEY_UP:
			look_up(camera, angle_inc);
			break;
		case GLFW_KEY_LEFT:
			look_left(camera, angle_inc);
			break;
		case GLFW_KEY_DOWN:
			look_up(camera, -angle_inc);
			break;
		case GLFW_KEY_RIGHT:
			look_left(camera, -angle_inc);
			break;
		case GLFW_KEY_W:
			move_forward(camera, distance_inc);
			break;
		case GLFW_KEY_S:
			move_forward(camera, -distance_inc);
			break;
		case GLFW_KEY_A:
			move_left(camera, distance_inc);
			break;
		case GLFW_KEY_D:
			move_left(camera, -distance_inc);
			break;
		case GLFW_KEY_SPACE:
			move_up(camera, distance_inc);
			break;
		case GLFW_KEY_ENTER:
			move_up(camera, -distance_inc);
			break;
	}

}
int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "shaders/vertex.shader";
	my_app.fragment_source_path 	= "shaders/fragment.shader";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_callback		= &key_callback;
	my_app.cursorpos_callback	= &cursorpos_callback;

	run(my_app);
}
