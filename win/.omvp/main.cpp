#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include "app.cpp"

GLuint vertex_array_object = 0;

void render(double current_time){
	GLfloat red[4]	 = {1.0f, 0.0f, 0.0f, 1.0f};
	GLfloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat gray[4]	 = {0.3f, 0.3f, 0.3f, 1.0f};

	glClearBufferfv(GL_COLOR, 0, black);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

void before_render_loop(){
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);
}

void after_render_loop(){
	glDeleteVertexArrays(1, &vertex_array_object);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}
int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "vertex.shader";
	my_app.fragment_source_path 	= "fragment.shader";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_callback		= &key_callback;

	run(my_app);
}
