#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <string>

#define EC(x)	while (glGetError() != GL_NO_ERROR);\
		x;\
		while ((error = glGetError()) != GL_NO_ERROR){\
		    printf("ERROR...");\
		    printf("Here is the error - %d\n", error);\
		    printf("%s\n", gluErrorString(error));\
		    exit(0);

struct Application {
	const char* vertex_source_path;
	const char* fragment_source_path;

	void (*before_render_loop)();
	void (*render)(double current_time);
	void (*after_render_loop)();
	void (*key_callback)(GLFWwindow*, int, int, int, int);
};

GLuint compile_shaders(const char* vs_path, const char* fs_path);
GLuint compile_single_shader(const char* source_path, GLenum shader_type);
GLFWwindow* init_window();

void error_callback(int error, const char* description){
	fprintf(stderr, "Error %d: %s\n", error, description);
}

int run(struct Application app){
	GLFWwindow* window = init_window();
	glewInit();
		GLenum error;
		while ((error = glGetError()) != GL_NO_ERROR)
		{
		    printf("GLEW ERROR...");
		    exit(0);
		}

	glfwSetErrorCallback(error_callback);

		while ((error = glGetError()) != GL_NO_ERROR)
		{
		    printf("GLFW ERROR...");
		    exit(0);
		}

	glfwSetKeyCallback(window, *app.key_callback);

	printf("Current GL version: %s\n", glGetString(GL_VERSION));
	printf("Current GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLuint program = compile_shaders(app.vertex_source_path, app.fragment_source_path);
		while ((error = glGetError()) != GL_NO_ERROR)
		{
		    printf("SHADER COMPILE ERROR...");
//		    exit(0);
		}
	(*app.before_render_loop)();
	while (!glfwWindowShouldClose(window)){
		glUseProgram(program);

		while ((error = glGetError()) != GL_NO_ERROR)
		{
		printf("here's program - %u", program);
		    printf("SOMG...");
		    printf("Here is the error - %d\n", error);
		    printf("%s\n", gluErrorString(error));
//		    exit(0);
		}
		(*app.render)(program);
		glfwSwapBuffers(window);
		glfwPollEvents();
		while ((error = glGetError()) != GL_NO_ERROR)
		{
		    printf("RENDER LOOP ERROR...");
		    printf("Here is the error - %d\n", error);
		    printf("%s\n", gluErrorString(error));
//		    exit(0);
		}
	}
	(*app.after_render_loop)();

	glDeleteProgram(program);
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}

GLuint compile_shaders(const char* vs_path, const char* fs_path){
	GLuint vertex_shader   = compile_single_shader(vs_path, GL_VERTEX_SHADER);
	GLuint fragment_shader = compile_single_shader(fs_path, GL_FRAGMENT_SHADER);

	GLuint program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);
	return program;
}

GLuint compile_single_shader(const char* source_path, GLenum shader_type){
	char buffer[1000];
	FILE* ss_fptr = fopen(source_path, "r");
	printf("ss_fptr - %x\n", ss_fptr);
	if (ss_fptr == NULL){printf("File doesn't exist?\n");}
	size_t chars_read = fread(buffer, sizeof(char), 1000, ss_fptr);
	if (chars_read >= 1000) {
		printf("Buffer too small");
		exit(1);
	}
	buffer[chars_read] = '\0';

	GLuint shader = glCreateShader(GL_VERTEX_SHADER);
	if (shader <= 0){exit(1);}
	char* buffer_ptr = buffer;
	glShaderSource(shader, 1, &(buffer_ptr), NULL);
	glCompileShader(shader);

	printf("#!Shader file:\n%s\n=====================\n", buffer);
	GLint log_length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);

	std::string str;
	str.reserve(log_length);
	glGetShaderInfoLog(shader, log_length, NULL, (GLchar *) str.c_str());
	printf("SHADER COMPILATION LOG:\n%s\n", str.c_str());

	GLint is_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
	if (!is_compiled){
		printf("\nSHADER COMPILATION FAILED!!!\n");
		exit(1);
	}
	return shader;
}

GLFWwindow* init_window(){
	if (!glfwInit()){
		fprintf(stderr, "GLFW failed to initialize.");
		exit(1);
	}

	GLFWwindow* window = glfwCreateWindow(1920, 1080, "Event Tester", NULL, NULL);
	if (!window){
		fprintf(stderr, "GLFW failed to create window.\n");
		exit(1);
	}
	glfwMakeContextCurrent(window);
	return window;
}
