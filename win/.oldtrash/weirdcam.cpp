#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <stdio.h>

struct Camera{
	glm::vec3 eye; //position
	glm::vec3 forward;
	glm::vec3 up;
};

glm::mat4x4 view_matrix(Camera const& cam){
//	return glm::translate(cam.eye) * glm::orientation(cam.forward, cam.up);
	glm::vec3 Up = {0.0f, 1.0f, 0.0f};
	return glm::lookAt(cam.eye, cam.eye + cam.forward, Up);
}

void look_left(Camera &cam, float const& angle){
	cam.forward = glm::rotate(cam.forward, angle, cam.up);
}

void look_up(Camera &cam, float const& angle){
	glm::vec3 right_normal = glm::cross(cam.forward, cam.up);
	cam.forward = glm::rotate(cam.forward, angle, right_normal);
	cam.up = glm::rotate(cam.up, angle, right_normal);
}

void translate(Camera &cam, glm::vec3 const& translation){
	cam.eye += translation;
}
void move_forward(Camera &cam, float const& distance){
	cam.eye += distance * cam.forward;
}










// //Ah I'll get gimbal locked...
