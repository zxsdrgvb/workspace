#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>
//#include <glm/gtx/transform.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.cpp"
#include "camera.cpp"
#include "torus.cpp"

size_t tdev = 1000;
size_t pdev = 200;
float small_r = 1;
float big_r = 2;

GLuint vertex_array_object = -1;
GLuint torus_buffer = -1;
Camera camera;

void print4vec(GLfloat *vec){
	printf("{%f, %f, %f, %f}\n", vec[0], vec[1], vec[2], vec[3]);
}

void vec3p(char *name, glm::vec3 const& vec){
	printf("%s - {%f, %f, %f}\n", name, vec.x, vec.y, vec.z);
}
void mat4p(char *name, glm::mat4 const& mat){
	printf("%s:\n", name);
	glm::mat4 tmat = glm::transpose(mat);
	for (int i = 0; i < 16; i++){
		printf("%f ", *(glm::value_ptr(tmat)+i));
		if (i % 4 == 3) printf("\n");
	}
	printf("\n");
}

void before_render_loop(){
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	glGenBuffers(1, &torus_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, torus_buffer);
	glBufferData(GL_ARRAY_BUFFER, 3*tdev * pdev * sizeof(float), NULL, GL_STATIC_DRAW);
	float* torus_buffer_ptr = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	torus_rings(torus_buffer_ptr, tdev, pdev, small_r, big_r);
	glUnmapBuffer(GL_ARRAY_BUFFER);

	camera.eye 	= {0.0f, 0.0f, -10.0f};
	camera.forward 	= {0.0f, 0.0f, 1.0f};
}

void render(GLuint program, double current_time){
//	vec3p("eye", camera.eye);
//	vec3p("forward", camera.forward);
//	vec3p("up", camera.up);

	GLfloat bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	glm::mat4 P = glm::perspectiveFov(120.0f, 1920.0f, 1080.0f, 0.01f, 100.0f);
//	mat4p("Projection Matrix", P);
	glm::mat4 V = view_matrix(camera);
//	mat4p("MV Mat", MV);
	glm::mat4 M = glm::mat4(1.0f);
	M = glm::rotate(M, (float) current_time, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 MVP = P * V * M;
//	mat4p("MVP", MVP);

	GLint iMVP = glGetUniformLocation(program, "MVP");
	//printf("iMVP - %d\n", iMVP);
	glUniformMatrix4fv(iMVP, 1, false, glm::value_ptr(MVP));

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawArrays(GL_POINTS, 0, tdev*pdev);
}


void after_render_loop(){
	glDeleteVertexArrays(1, &vertex_array_object);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	float angle_inc = 0.05;
	float distance_inc = 0.5;
	switch(key){
		case GLFW_KEY_UP:
			look_up(camera, angle_inc);
			break;
		case GLFW_KEY_LEFT:
			look_left(camera, angle_inc);
			break;
		case GLFW_KEY_DOWN:
			look_up(camera, -angle_inc);
			break;
		case GLFW_KEY_RIGHT:
			look_left(camera, -angle_inc);
			break;
		case GLFW_KEY_W:
			move_forward(camera, distance_inc);
			break;
		case GLFW_KEY_S:
			move_forward(camera, -distance_inc);
			break;
		case GLFW_KEY_A:
			move_left(camera, distance_inc);
			break;
		case GLFW_KEY_D:
			move_left(camera, -distance_inc);
			break;
		case GLFW_KEY_SPACE:
			move_up(camera, distance_inc);
			break;
		case GLFW_KEY_ENTER:
			move_up(camera, -distance_inc);
			break;
	}

}
int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "shaders/vertex.shader";
	my_app.fragment_source_path 	= "shaders/fragment.shader";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_callback		= &key_callback;

	run(my_app);
}
