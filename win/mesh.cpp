#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>
//#include <glm/gtx/transform.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.cpp"
#include "camera.cpp"
#include "torus.cpp"

size_t theta_divisions = 10;
size_t phi_divisions = 20;
float small_r = 1;
float big_r = 2;

GLuint vertex_array_object = -1;
GLuint torus_buffer = -1;
GLuint index_buffer = -1;
Camera camera;
GLuint PRIMITIVE_RESTART_INDEX = 0xFFFFFFFF;
void print4vec(GLfloat *vec){
	printf("{%f, %f, %f, %f}\n", vec[0], vec[1], vec[2], vec[3]);
}

void vec3p(char *name, glm::vec3 const& vec){
	printf("%s - {%f, %f, %f}\n", name, vec.x, vec.y, vec.z);
}
void mat4p(char *name, glm::mat4 const& mat){
	printf("%s:\n", name);
	glm::mat4 tmat = glm::transpose(mat);
	for (int i = 0; i < 16; i++){
		printf("%f ", *(glm::value_ptr(tmat)+i));
		if (i % 4 == 3) printf("\n");
	}
	printf("\n");
}

void fill_phi_loop_indices(uint* storage_ptr);

void before_render_loop(){
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	glGenBuffers(1, &torus_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, torus_buffer);
	glBufferData(GL_ARRAY_BUFFER, 3*theta_divisions * phi_divisions * sizeof(float), NULL, GL_STATIC_DRAW);
	float* torus_buffer_ptr = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	torus_rings(torus_buffer_ptr, theta_divisions, phi_divisions, small_r, big_r);
	glUnmapBuffer(GL_ARRAY_BUFFER);


	glGenBuffers(1, &index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (theta_divisions + 1) * phi_divisions * sizeof(uint), NULL, GL_STATIC_DRAW);
	uint* index_buffer_ptr = (uint*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
	fill_phi_loop_indices(index_buffer_ptr);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	camera.eye 	= {0.0f, 0.0f, -10.0f};
	camera.forward 	= {0.0f, 0.0f, 1.0f};
}

void render(GLuint program, double current_time){
	GLfloat bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	glm::mat4 P = glm::perspectiveFov(120.0f, 1920.0f, 1080.0f, 0.01f, 100.0f);
	glm::mat4 V = view_matrix(camera);
	glm::mat4 M = glm::mat4(1.0f);
	//M = glm::rotate(M, (float) current_time, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 MVP = P * V * M;

	GLint iMVP = glGetUniformLocation(program, "MVP");
	glUniformMatrix4fv(iMVP, 1, false, glm::value_ptr(MVP));

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);


//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(PRIMITIVE_RESTART_INDEX);
	glDrawElements(GL_LINE_LOOP, (theta_divisions+1)*phi_divisions, GL_UNSIGNED_INT, (void*) 0);
	glDisable(GL_PRIMITIVE_RESTART);

	uint i = 0;
	while (i < theta_divisions){
		glDrawArrays(GL_LINE_LOOP, phi_divisions*i, phi_divisions);
		i++;
	}
}



void after_render_loop(){
	glDeleteVertexArrays(1, &vertex_array_object);
}

void fill_phi_loop_indices(uint* ibufferptr){
	uint i = 0;
	for (uint phi_i = 0; phi_i < phi_divisions; phi_i++){
		printf("phi_i - %d\n", phi_i);
		for (uint theta_i = 0; theta_i < theta_divisions; theta_i++){
			ibufferptr[i] = phi_divisions*theta_i + phi_i;
			printf("ibufferptr[%d] = %d %d %d\n", i, phi_i, theta_i, phi_divisions*theta_i + phi_i);
			i++;
		}
		ibufferptr[i] = PRIMITIVE_RESTART_INDEX;
		i++;
	}
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	float angle_inc = 0.05;
	float distance_inc = 0.5;
	switch(key){
		case GLFW_KEY_UP:
			look_up(camera, angle_inc);
			break;
		case GLFW_KEY_LEFT:
			look_left(camera, angle_inc);
			break;
		case GLFW_KEY_DOWN:
			look_up(camera, -angle_inc);
			break;
		case GLFW_KEY_RIGHT:
			look_left(camera, -angle_inc);
			break;
		case GLFW_KEY_W:
			move_forward(camera, distance_inc);
			break;
		case GLFW_KEY_S:
			move_forward(camera, -distance_inc);
			break;
		case GLFW_KEY_A:
			move_left(camera, distance_inc);
			break;
		case GLFW_KEY_D:
			move_left(camera, -distance_inc);
			break;
		case GLFW_KEY_SPACE:
			move_up(camera, distance_inc);
			break;
		case GLFW_KEY_ENTER:
			move_up(camera, -distance_inc);
			break;
	}

}
int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "shaders/vertex.shader";
	my_app.fragment_source_path 	= "shaders/fragment.shader";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_callback		= &key_callback;

	run(my_app);
}
