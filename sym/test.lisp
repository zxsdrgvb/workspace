#|
Symbol trees
	+
       /  \
      +    *
     / \  / \
    4  5  6  7
would be 4 + 5 + 6 * 7, which is exactly how lisp normally stores these expressions:
(+ (+ 4 5) (* 6 7))
Well not exactly, but the idea's the same, and of course we have general addition instead of binary.

Differentiating function trees
We'll of course need to end with a variable instead. (f y) where y is some expression of x becomes:
(f y)' = (* (f' y) (f y'))
and for binary functions we have
d/dx[f(a(x), b(x))] = &f/&a * da/dx + &f/&b * db / dx
where & means partial, so we have to store the gradient of f, i.e all the partials.
(f a b)' = (+ (* (f1 a b) a') (* (f2 a b) b'))
In specific cases:
(+ a b)' = (+ a' b')
(- a b)' = (- a' b')
(* a b)' = (+ (* a' b) (* a b'))

I think I'll leave a raw number to mean multiply? Actually no, I'll use a raw number to mean power:
(n a) := a^n
Since it's common for n to be just an atomic number. The general exponentiation:
(** n a) = a ** n?
That would be confusing, I'll switch it then? I guess I'll mean
(a n) := a ^ n
Well, we'll see.
|#

(defun diff (tree)
  (if (atom tree)
      (cond ((null tree) nil)
	    ((eq tree 'x) 1)
	    (otherwise 0))
      (case (first tree)
	    ('+ `(+ ,(diff (second tree)) ,(diff (third tree))))
	    ('* `(+ (* ,(diff (second tree)) ,(third tree)) (* ,(second tree) ,(diff (third tree))))))))

(defun givex (tree x)
  (if (atom tree)
      (cond ((null tree) nil)
	    ((eq tree 'x) x)
	    ((numberp tree) tree))
      (case (first tree)
	    ('+ (+ (givex (second tree) x) (givex (third tree) x)))
	    ('* (* (givex (second tree) x) (givex (third tree) x))))))
