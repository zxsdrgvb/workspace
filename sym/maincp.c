#include <stdio.h>

struct Node {
	struct Node *up, *down, *left, *right;
	int flags;
	union {
		unsigned int id;
		float value;
	} value;
}

struct Node *parse_lisp(char* text){
	struct Node head_node;
	//head_node.up = 0xFFFFFFFF;
	unsigned int i = 0;
	struct Node* current_node = &head_node;
	bool awaiting_function = false;
	bool make_down;
	while(text[i] != \0;){
		switch(text[i]) { //supposed to do text[i] then i++
			case ' ':
			case '\n':
			case '\r':
				i++;
				continue;
			case '(':
				struct Node *new_node = alloc_new_node();
				current_node->down = new_node;
				new_node->up = current_node;
				current_node = new_node;
				i++;
				continue;
			case ')':
				current_node.right = NULL;
				current_node.down = NULL;
				if (current_node == &head_node){
					printf("Traversed! Back to head node.");
					return *head_node;
				}
				//we don't need to worry about reaching the head_node here
				//since the head node has no siblings
				while(current_node.up == NULL) current_node = current_node.left;
				i++;
				continue;
			case '\0':
				printf("Parse Error! Missing right bracket.");
				return NULL;
			otherwise:
				if(awaiting_function){
					if (current_node.right != NULL) {
						printf("Parse Error on %c! Function wihout (, eg: (+ + 1 1) instead of (+ 1 1), could be accidental whitespace.\n", text[--i]);
						return NULL;
					}
					current_node->value = NULL;
					change_to_function_node(current_node); //do i even need this flag?
					unsigned int whitespace_location = 0;
					unsigned int function_id = get_function_id(text + i, &whitespace_location); //function that gets function id by checking the text until whitespace
					if (whitespace_location == 0){
						printf("Function id not found!");
						return NULL;
					}
					current_node.id = function_id;

					i += whitespace_location;
					awaiting_function = false;
					struct Node *new_node = alloc_new_node();
					current_node->down = new_node;
					new_node->up = current_node;
					current_node = new_node;
					continue;
				} else { //expecting an atom, so a variable or a float
					change_to_value_node(current_node); //prob dont need this flag
					unsigned int whitespace_location = 0;
					write_value(text + i, &whitespace_location, current_node);
					if (whitespace_location == 0){
						printf("Atom parsing error.");
						return NULL;
					}
					i += whitespace_location;

					struct Node *new_node = alloc_new_node();
					current_node->right = new_node;
					new_node->left = current_node;
					current_node = new_node;
		}
	}

}


int main(){

	return 0;
}

