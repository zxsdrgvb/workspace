#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

union Value{
	unsigned int id;
	float constant;
};

struct Node {
	struct Node *up, *down, *left, *right;
	int flags;
	union Value value;
};

int function_node_flag = 1 << 0;
int variable_node_flag = 1 << 1;
void change_to_function_node(struct Node *node){
	node->flags ^= function_node_flag;
}
void change_to_value_node(struct Node *node){
	node->flags &= ~function_node_flag;
}
unsigned int get_function_id(const char *text_pointer, char *whitespace_location_ptr){
	unsigned int i = 0;
	switch(text_pointer[i]){
		case '+':
			*whitespace_location_ptr = i + 1;
			return 1;
		case '*':
			*whitespace_location_ptr = i + 1;
			return 2;
		case '-':
			*whitespace_location_ptr = i + 1;
			return 3;
		case '/':
			*whitespace_location_ptr = i + 1;
			return 4;
	}
}
void write_value(const char *text_pointer, char *whitespace_location_ptr, struct Node *node){
	//unsigned int i = 0;
	*whitespace_location_ptr = 1;
	node->value = (union Value) ((float) (text_pointer[0] - 48));
}

struct Node *alloc_new_node(){
	struct Node *new_node = malloc(sizeof(struct Node));
	new_node->flags = 0;
}

struct Node *parse_lisp(char* text){
	struct Node *head_node_ptr = alloc_new_node();
	struct Node head_node = *head_node_ptr;
	//head_node->up = 0xFFFFFFFF;
	unsigned int i = 0;
	struct Node* current_node = &head_node;
	bool awaiting_function = false;
	bool awaiting_first_node = false;
	bool make_down;
	while(text[i] != '\0'){
		switch(text[i]) { //supposed to do text[i] then i++
			case ' ':
			case '\n':
			case '\r':
				i++;
				continue;
			case '(':
				awaiting_function = true;
				i++;
				continue;
			case ')':
				current_node->right = NULL;
				if (current_node == &head_node){
					printf("Traversed! Back to head node.\n");
					return head_node_ptr;
				}
				//we don't need to worry about reaching the head_node here
				//since the head node has no siblings
				while(current_node->up == NULL) current_node = current_node->left;
				current_node = current_node->up;
				i++;
				continue;
			case '\0':
				printf("Parse Error! Missing right bracket.");
				return NULL;
			default:
				if(awaiting_function){
					if (current_node->right != NULL) {
						printf("Parse Error on %c! Function wihout (, eg: (+ + 1 1) instead of (+ 1 1). Could be accidental whitespace.\n", text[--i]);
						return NULL;
					}
					//current_node->value = NULL;
					//change_to_function_node(current_node); //do i even need this flag?
					unsigned int whitespace_location = 0;
					unsigned int function_id = get_function_id(text + i, &whitespace_location); //function that gets function id by checking the text until whitespace
					if (whitespace_location == 0){
						printf("Function id not found!");
						return NULL;
					}
					current_node->value.id = function_id;

					i += whitespace_location;
					awaiting_function = false;
					awaiting_first_node = true;
					continue;
				} else { //expecting an atom, so a variable or a float
					struct Node *new_node = alloc_new_node();
					if (awaiting_first_node){
						current_node->down = new_node;
						new_node->up = current_node;
						awaiting_first_node = false;
					} else {
						current_node->right = new_node;
						new_node->left = current_node;
					}
					current_node = new_node;

					//change_to_value_node(current_node); //prob dont need this flag
					unsigned int whitespace_location = 0;
					write_value(text + i, &whitespace_location, current_node);
					if (whitespace_location == 0){
						printf("Atom parsing error.");
						return NULL;
					}

					i += whitespace_location;
					continue;
				}
		}
	}
}


int main(){
	parse_lisp("(+ 1 1)");
	return 0;
}

