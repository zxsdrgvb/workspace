#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.cpp"
#include "camera.cpp"


GLuint vertex_array_object = 0;
Camera camera;

void print4vec(GLfloat *vec){
	printf("{%f, %f, %f, %f}\n", vec[0], vec[1], vec[2], vec[3]);
}

void render(GLuint program, double current_time){
	GLfloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	GLfloat my_offset[] = {	std::cos(current_time)*0.5f,
				std::sin(current_time)*0.5f,
				0,
				0};
//	print4vec(my_offset);
	glVertexAttrib4fv(0, my_offset);

//	glm::mat4x4 MVP = view_matrix(camera) * glm::perspectiveFov(120, 1920, 1080, 1, 100);
	auto MVP = view_matrix(camera) * glm::perspectiveFov(120, 1920, 1080, 1, 100);
	GLint iMVP = glGetUniformLocation(program, "MVP");
	glUniformMatrix4fv(iMVP, 1, false, glm::value_ptr(MVP));



	glClearBufferfv(GL_COLOR, 0, black);
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

void before_render_loop(){
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	camera.eye 	= {0.0f, 0.0f, 0.0f};
	camera.forward 	= {0.0f, 0.0f, 1.0f};
	camera.up 	= {0.0f, 1.0f, 0.0f};
}

void after_render_loop(){
	glDeleteVertexArrays(1, &vertex_array_object);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

}
int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "shaders/vertex.shader";
	my_app.fragment_source_path 	= "shaders/fragment.shader";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_callback		= &key_callback;

	run(my_app);
}
