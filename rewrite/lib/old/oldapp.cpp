#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <string>
#include <signal.h>

#define EC(x)	while (glGetError() != GL_NO_ERROR);\
		x\
		while ((error = glGetError()) != GL_NO_ERROR){\
		    printf("ERROR...");\
		    printf("Here is the error - %d\n", error);\
		    printf("%s\n", gluErrorString(error));\
		    printf("FILE: %s, LINE: %d\n", __FILE__, __LINE__);\
		}

GLenum error;
char key_array[GLFW_KEY_LAST + 1] = {0};

struct Application {
	const char* vertex_source_path;
	const char* fragment_source_path;

	void (*before_render_loop)();
	void (*render)(GLFWwindow* window, GLuint program, double current_time);
	void (*after_render_loop)();
	void (*key_update)(char* key_array);
	void (*cursorpos_callback)(GLFWwindow*, double xpos, double ypos);
};

GLuint compile_shaders(const char* vs_path, const char* fs_path);
GLuint compile_single_shader(const char* source_path, GLenum shader_type);
GLFWwindow* init_window();

void error_callback(int error, const char* description){
	fprintf(stderr, "Error %d: %s\n", error, description);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
		return;
	}
	if (key == GLFW_KEY_UNKNOWN){return;} //GLFW_KEY_UNKNOWN is -1
	if (action == GLFW_PRESS) {	key_array[key] = (char) true;}
	if (action == GLFW_RELEASE){ 	key_array[key] = (char) false;}
}

int run(struct Application app){
EC(	GLFWwindow* window = init_window();)
EC(	glewInit();)

EC(	glfwSetErrorCallback(error_callback);)
EC(	glfwSetKeyCallback(window, key_callback);)
EC(	glfwSetCursorPosCallback(window, *app.cursorpos_callback);)
EC(	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);)
EC(	glfwSetCursorPos(window, 0, 0);)

EC(	printf("Current GL version: %s\n", glGetString(GL_VERSION));)
EC(	printf("Current GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));)

EC(	GLuint program = compile_shaders(app.vertex_source_path, app.fragment_source_path);)
EC(	(*app.before_render_loop)();)
	while (!glfwWindowShouldClose(window)){
EC(		glUseProgram(program);)
EC(		(*app.key_update)(key_array);)
EC(		(*app.render)(window, program, glfwGetTime());)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
EC(	(*app.after_render_loop)();)

	glDeleteProgram(program);
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}

GLuint compile_shaders(const char* vs_path, const char* fs_path){
EC(	GLuint vertex_shader   = compile_single_shader(vs_path, GL_VERTEX_SHADER);)
EC(	GLuint fragment_shader = compile_single_shader(fs_path, GL_FRAGMENT_SHADER);)

EC(	GLuint program = glCreateProgram();)
EC(	glAttachShader(program, vertex_shader);)
EC(	glAttachShader(program, fragment_shader);)
EC(	glLinkProgram(program);)
	printf("Really? No link problems? Gesundheit\n\n\n\n\n\n\n\n\n");
	char buffer[1000];
	int length;
	glGetProgramInfoLog(program, 1000, &length, buffer);
	printf("PROGRAM info log - %s\n", buffer);
	return program;
}

GLuint compile_single_shader(const char* source_path, GLenum shader_type){
	char buffer[1000];
	FILE* ss_fptr = fopen(source_path, "r");
	printf("ss_fptr - %x\n", ss_fptr);
	if (ss_fptr == NULL){printf("File doesn't exist?\n");}
	size_t chars_read = fread(buffer, sizeof(char), 1000, ss_fptr);
	if (chars_read >= 1000) {
		printf("Buffer too small");
		exit(1);
	}
	buffer[chars_read] = '\0';

	GLuint shader = glCreateShader(shader_type);
	if (shader <= 0){exit(1);}
	char* buffer_ptr = buffer;
	glShaderSource(shader, 1, &(buffer_ptr), NULL);
	glCompileShader(shader);

	printf("#!Shader file:\n%s\n=====================\n", buffer);
	GLint log_length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);

	std::string str;
	str.reserve(log_length);
	glGetShaderInfoLog(shader, log_length, NULL, (GLchar *) str.c_str());
	printf("SHADER COMPILATION LOG:\n%s\n", str.c_str());

	GLint is_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
	if (!is_compiled){
		printf("\nSHADER COMPILATION FAILED!!!\n");
		exit(1);
	}
	return shader;
}

GLFWwindow* init_window(){
	if (!glfwInit()){
		fprintf(stderr, "GLFW failed to initialize.");
		exit(1);
	}
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );

	GLFWwindow* window = glfwCreateWindow(1920, 1080, "Event Tester", NULL, NULL);
	if (!window){
		fprintf(stderr, "GLFW failed to create window.\n");
		exit(1);
	}
	glfwMakeContextCurrent(window);
	return window;
}
