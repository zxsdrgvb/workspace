#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <string>
#include <signal.h>

const int NO_OF_SHADERS = 4;
const bool debug_mode = true;

struct Application {
	const GLenum shader_types[NO_OF_SHADERS];
	const char* shader_source_paths[NO_OF_SHADERS];

	void (*before_render_loop)();
	void (*render)(GLFWwindow* window, GLuint program, double current_time);
	void (*after_render_loop)();
	void (*key_update)(char* key_array);
	void (*cursorpos_callback)(GLFWwindow*, double xpos, double ypos);

	char* shader_sources[NO_OF_SHADERS];
	GLuint program;
	GLuint shader[NO_OF_SHADERS];
	GLFWwindow* window;
	char key_array[GLFW_KEY_LAST + 1];
};

void GLAPIENTRY MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);

void init_window(struct Application *app){
	if (!glfwInit()){
		fprintf(stderr, "GLFW failed to initialize.");
		exit(1);
	}
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
	if (debug_mode)	{
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); //I know it's available for my system
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(MessageCallback, NULL);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
	}


	app->window = glfwCreateWindow(1920, 1080, "Event Tester", NULL, NULL);
	if (!(app->window)){
		fprintf(stderr, "GLFW failed to create window.\n");
		exit(1);
	}
	glfwMakeContextCurrent(app->window);
	glfwSetUserPointer(window, app);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
		return;
	}
	struct Application* app = glfwGetUserPointer(window);
	if (key == GLFW_KEY_UNKNOWN){return;} //GLFW_KEY_UNKNOWN is -1
	if (action == GLFW_PRESS) {	app->key_array[key] = (char) true;}
	if (action == GLFW_RELEASE){ 	app->key_array[key] = (char) false;}
}

int run(struct Application* app){
	init_window();
	glewInit();

	glfwSetErrorCallback(error_callback);
	glfwSetKeyCallback(app->window, key_callback);
	glfwSetCursorPosCallback(app->window, app->cursorpos_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(window, 0, 0);

	printf("Current GL version: %s\n", glGetString(GL_VERSION));
	printf("Current GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	compile_shaders(app);
	(*(app->before_render_loop))();
	while (!glfwWindowShouldClose(window)){
		glUseProgram(app->program);
		(*app->key_update)(key_array);
		(*app->render)(window, program, glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	(*app->after_render_loop)();

	glDeleteProgram(app->program);
	glfwDestroyWindow(app->window);
	glfwTerminate();
	return 0;
}

GLuint compile_shaders(struct Application* app){
	app->program = glCreateProgram();
	if (program <= 0) exit(1);
	for (int shader_index = 0; shader_index < NO_OF_SHADERS; shader_index++){
		compile_single_shader(app, shader_index);
		glAttachShader(app->program, app->shader[shader_index]);
	}
	glLinkProgram(app->program);
}

void compile_single_shader(struct Application* app, int shader_index){
	const char* source_path = app->shader[shader_index];
	GLenum shader_type 	= app->shader_types[shader_index];
	FILE* fd = fopen(source_path, "r");
	if (!fd){
		fprintf(stderr, "ERROR: %s failed to open. Doesn't exist?\n", source_path);
		exit(1);
	}

	//getting file size
	fseek(fd, 0L, SEEK_END);
	long filesize = ftell(fd); //in no. of bytes, so no. of chars
	fseek(fd, 0L, SEEK_SET);
	printf("Size of %s is %d bytes.\n", source_path, filesize);

	//mallocing buffer
	char* buffer = malloc(filesize + 1);
	if (!buffer){
		fprintf(stderr, "Buffer malloc for %d bytes failed.\n", filesize);
		exit(1);
	}
	//reading into buffer and null terminating
	size_t bytes_read = fread(buffer, sizeof(char), filesize, fd);
	if (bytes_read != filesize){
		fprintf(stderr, "%d bytes read, not equal to filesize: %d bytes.\n", bytes_read, filesize);
		exit(1);
	}
	buffer[filesize] = '\0'; //OpenGL requires this or else I'll have to specify filesize
	app->shader_source[shader_index] = buffer;

	//compiling shader
	GLint shader = glCreateShader(shader_type);
	if (shader <= 0) {
		fprintf(stderr, "Shader creation failed.\n");
		exit(1);
	}
	glShaderSource(shader, 1, &(app->shader_source[shader_index]), NULL);
	glCompileShader(shader);
	app->shader[shader_index] = shader;

	//printing buffer to terminal
	printf("----------%s----------\n%s\n----------%s----------\n\n",
			source_path,
			buffer,
			source_path);

	//printing debug info
	GLint log_length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);
	char* log_buffer = malloc(log_length);
	if (!log_buffer) {
		fprintf(stderr, "Log buffer malloc failed for %d bytes.\n", log_length);
		exit(1);
	}
	glGetShaderInfoLog(shader, log_length, NULL, log_buffer);
	printf("COMPILATION LOG FOR %s:\n%s\n", source_path, log_buffer);
	free(log_buffer);

	//printing compilation status
	GLint is_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
	if (is_compiled){
		printf("Compilation of %s successful.\n", source_path);
	} else {
		fprintf(stderr, "COMPILATION OF %s FAILED.\n", source_path);
		exit(1);
	}
}

void GLAPIENTRY
MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}
