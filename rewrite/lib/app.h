#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <string>
#include <signal.h>

const int NO_OF_SHADERS = 2;

struct Application {
	GLenum shader_types[NO_OF_SHADERS];
	const char* shader_source_paths[NO_OF_SHADERS];

	void (*before_render_loop)();
	void (*render)(struct Application* app);
	void (*after_render_loop)();
	void (*key_update)(char* key_array);
	void (*cursorpos_callback)(GLFWwindow*, double xpos, double ypos);

	char* shader_sources[NO_OF_SHADERS];
	GLuint program;
	GLuint shader[NO_OF_SHADERS];
	GLFWwindow* window;
	char key_array[GLFW_KEY_LAST + 1];
};

int run(struct Application* app);
void init_window(struct Application *app);
void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
void compile_shaders(struct Application* app);
void compile_single_shader(struct Application* app, int shader_index);

