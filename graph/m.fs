#version 400 core

out vec4 color;

vec4 white = vec4(1, 1, 1, 1);
vec4 black = vec4(0, 0, 0, 1);

uniform vec4 boundary;
uniform uint max;
uniform float zx0;
uniform float zy0;

float f(float x, float y, float a, float b){
//	return (x - a)*(x - a) + (y - b)*(y - b) - 1;
	return x*x*x - y*y - 4*x + 3;
}

float epsilon = float(1)/max;
float scale = 2;
void main(){
	float zx = (gl_FragCoord.x/1920)*(boundary.z - boundary.x) + boundary.x;
	float zy = (gl_FragCoord.y/1080)*(boundary.y - boundary.w) + boundary.w;
	float cx = zx0;
	float cy = zy0;
	uint i = 0;
	if (abs(f(zx, zy, cy, -cx)) < epsilon) i = 1;
	color = vec4(i, i, i, 1);
	//color = vec4(float(i) / float(max) * vec3(1, 1, 1), 1);
}
