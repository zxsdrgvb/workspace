(Before Feb 7th)
I didn't really have a direct use for learning about uniform packing and there was nothing interesting about textures, so I thought I would try to make a simple, rectangular "FPS": I say FPS in quotes because I'm not sure if I'll actually add a gun ever, but what I want first of all is:

1) Floors, jumping, gravity
2) Walls and collision
3) A map editor, preferably somewhat easy to use
4) Textures on the floors and the walls (and ceiling)
5) Sprites (with textures?)

I think that will be enough to start with. I already have the code to setup my window and handle my keys, and to move my camera and so on, and initially the floor and gravity is pretty easy, as are the walls if I implement only square rooms, i.e no tilting walls. So I'll just hold all the triangles in my memory and check if the body is on one side or the other, etc? Seems confusing.

New plan: Minecraft clone (Embarrassing?)
I don't want to deal with collision so...

- Chunks will be 16x16x16. I'm pretty sure MC uses chunks to mean we'll do all the tick-updating for a chunk at one time. For me it'll just be a shorthand for the viewing option... Actually I don't really need chunks.
- Blocks will just be stored as a 3 dimensional array of unsigned int's. Currently I could get away with just a byte (i.e a char)... actually yeah, 4 billion is too far. Currently 256 is probably enough for me, if I really need it then I could use 2 bytes, and I forgot what that's called, I'm pretty sure that's a short int. That'll have 2^16 which is more than 64k (by 2.4%), so that's much more than enough, and I can store mob information or something like that if I needed to. I don't really have a choice of using a byte and a half. For now they'll be nothing but block information.

0 - air
1 - dirt
2 - grass
3 - stone
4 - cobblestone
5 - wood?

I'll keep the wood away for now. But when I really think about, does even mojang have more than 256 blocks? I'm going to use a char for now because I'm realistically not going to use any more than that, even including all the rotations I'm not going to make it that far. It won't be too hard changing it if I ever needed to. (Actually once I think about all the types of colored glass, nevermind it's definitely more than 256).

Then we have the rendering. I remember watching a video of someone doing this an year or two ago but his problem was that of course rendering every triangle when you only see the surface ones was a waste. The simplest way is if I had some kind of function to check if a coordinate was visible to the player. Could I reverse my projection matrix? I heard there was something like that. Of course it squishes everything so I'll get a line back. And I just check if a block is there along the way? I don't know much about ray tracing. Seems confusing, but I don't see any other way with this method. What the person used in the video was that if the vertices were shared he removed faces? I think. Let's take an example. The blocks in a chunk will just be stored as a char[16][16][16], which is 4096 bytes, so if we take a box of boxes in the centre and we try to draw it, I'll start with 0,0,0 then move along x... so if they're next to each other in x that's fine but along y? Won't work. Or if every single block around this one, all 8, are filled then I won't draw it. I think that's fine. So if it's air or if it's buried I don't draw it. I'll try to do that then.

Mon Feb 13 10:17:40 PM IST 2023
Update - I finished making the box thing, it's pretty simple to add the "if every block around it is filled, don't draw" simply by continuing. So the actual graphics part is done (except for lighting I don't know how to do that yet) and when I think about it dealing with chunks and procedural generation is actually much more complicated than collision detection. And in the end a minecraft clone seems boring. Not to mention I'll need to add a way to detect which block I'm pointing at? Which I assume is done with the reverse projection, but it's not such an issue since you only need to go at maximum 3 or 4 blocks away. So return to the original idea. Guess I'll read about collision because I can't figure a way of implementing it by myself.

10:36:42 PM
I realised I could just remove the -x face if there's a block to the -x. If I did that for +x as well then neither block would draw that face, so I'll only do it for -x, -y and -z. It's better in some scenarios. But unfortunately if there's a gigantic block then the inside won't just vanish, I'll still be drawing half the faces. So I should do both in tandem. Unfortunately it's not as simple as just checking if everything is around it anymore. But removing, completely, duplicate triangle drawing is worth it. I'm not the one who's going to do it though. Maybe later. I'm also going to need to learn how to do textures, but honestly that seems like a headache to get working efficiently, I'm not sure. I'll leave that until later too. To learn about textures I'll make a simple doom-clone like I was talking about. It's much easier to make an FPS because I don't feel like cheating if I look something up. If I look up chunk handling for minecraft, that feels like cheating, since minecraft is a game but FPS is a genre.
