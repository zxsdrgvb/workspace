#version 330 core

in vec4 pos_fs;
out vec4 color;
void main(){
	//color = vec4(0, 1, 0, 1);
	//color = vec4(pos_fs.xyz / 10, 1);
	int step = 50;
	color = 2*vec4((int(pos_fs.x)) % step, (int(pos_fs.y)) % step, (int(pos_fs.z)) % step, step) / step;
}
