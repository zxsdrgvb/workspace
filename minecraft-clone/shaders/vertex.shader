#version 330 core

in vec4 position;
uniform mat4 MVP;

out float vertex_id;
out vec4 pos_fs;
void main(){
	vertex_id = gl_VertexID;
//	pos_z = position.z;
	gl_Position = MVP*position;
	pos_fs = position;
}
