#version 330 core

//layout (location = 0) in vec4 my_offset_but_glsl;

uniform mat4 MVP;

out vec4 vs_color;
void main(void)
{
	// Declare a hard-coded array of positions
	const vec4 vertices[3] = vec4[3](	vec4( 0.25, -0.25, 1.0, 1.0),
						vec4(-0.25, -0.25, 1.0, 1.0),
						vec4( 0.25, 0.25, 1.0, 1.0));
	// Index into our array using gl_VertexID
	gl_Position = MVP*(vertices[gl_VertexID]);
	vs_color = 0.5 * gl_Position + 0.5;
}
