#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>
//#include <glm/gtx/transform.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.cpp"
#include "lib/camera.cpp"

GLuint vertex_array_object = -1;
GLuint index_buffer = -1;
Camera camera;

unsigned int block_i = 0;

void printf_arr(float* ptr, size_t length){
	for (int i = 0; i < length; i++){
		printf("%d: %f\n", i, ptr[i]);
	}
	printf("\n");
}
void before_render_loop(){
	glGenVertexArrays(1, &vertex_array_object);
	glBindVertexArray(vertex_array_object);

	char chunk[16][16][16] = {0};
	chunk[1][1][9] = 1;
	chunk[1][6][1] = 1;
	chunk[1][1][1] = 1;

	for (char i = 0; i < 16; i++){
	for (char j = 0; j < 16; j++){
	for (char k = 0; k < 16; k++){
//			chunk[i][j][k] = 1;
		chunk[i][j][3] = i % 2 - j % 2;
		chunk[i][j][10] = 1;

	}}}
	for (int k = 0; k < 16; k++){
		chunk[9][9][k] = 1;
	}



//	chunk[5][5][5] = 1;
	GLuint chunk_buffer = -1;
	glGenBuffers(1, &chunk_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, chunk_buffer);
	glBufferData(GL_ARRAY_BUFFER, 24*16*16*16*sizeof(float), NULL, GL_STATIC_DRAW);
	float* chunk_buffer_ptr = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	GLuint index_buffer = -1;
	glGenBuffers(1, &index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36*16*16*16*sizeof(unsigned int), NULL, GL_STATIC_DRAW);
	unsigned int* index_buffer_ptr = (unsigned int*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

	for (char i = 0; i < 16; i++){
	for (char j = 0; j < 16; j++){
	for (char k = 0; k < 16; k++){
		if (chunk[i][j][k] == 0) continue;
		printf("nonzero %d %d %d\n", i, j, k);

		unsigned int stack_i = 24*block_i;
		for (char is = 0; is < 2; is++){
		for (char js = 0; js < 2; js++){
		for (char ks = 0; ks < 2; ks++){
			chunk_buffer_ptr[stack_i]	= (float) (i + is);
			chunk_buffer_ptr[stack_i + 1] 	= (float) (j + js);
			chunk_buffer_ptr[stack_i + 2] 	= (float) (k + ks);
			stack_i += 3;
		}}}
/*
		unsigned int index_draw_list[36] = {1, 2, 3,
						    3, 2, 4,

						    1, 2, 5,
						    5, 2, 6,

						    1, 3, 5,
						    5, 3, 7,

						    2, 4, 6,
						    6, 4, 8,

						    6, 8, 7,
						    7, 8, 5,

						    5, 6, 7,
						    7, 6, 8};
*/
/*
		unsigned int index_draw_list[36] = {1, 0, 3,
						    3, 0, 2,

						    4, 5, 6,
						    6, 5, 7,

						    3, 2, 7,
						    7, 2, 6,

						    0, 1, 4,
						    4, 1, 5,

						    7, 5, 3,
						    3, 5, 1,

						    2, 0, 6,
						    6, 0, 4};

*/
		unsigned int index_draw_list[36] = {0, 1, 3,
						    0, 3, 2,

						    5, 4, 6,
						    5, 6, 7,

						    2, 3, 7,
						    2, 7, 6,

						    1, 0, 4,
						    1, 4, 5,

						    5, 7, 3,
						    5, 3, 1,

						    0, 2, 6,
						    0, 6, 4};



		for (unsigned int i = 0; i < 36; i++){
			index_buffer_ptr[block_i*36 + i] = 8*block_i + index_draw_list[i];
		}

		block_i += 1;
	}}}
	//printf_arr(chunk_buffer_ptr, 24*block_i);
	float* ptr = chunk_buffer_ptr;
	for (int i = 0; i < 8*block_i; i++){
		printf("%d: (%f, %f, %f)\n", i, ptr[3*i], ptr[3*i + 1], ptr[3*i + 2]);
	}
	printf("\n");
	unsigned int* aptr = index_buffer_ptr;
	for (int i = 0; i < 12*block_i; i++){
		printf("%d: (%d, %d, %d)\n", i, aptr[3*i], aptr[3*i + 1], aptr[3*i + 2]);
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	camera.eye 	= {0.0f, 0.0f, -10.0f};
	camera.forward 	= {0.0f, 0.0f, 1.0f};
}

glm::mat4 makeMVP(GLFWwindow* window);

void render(GLFWwindow* window, GLuint program, double current_time){
	GLfloat bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	//set uniforms
	glm::mat4 MVP = makeMVP(window);
	GLint iMVP = glGetUniformLocation(program, "MVP");
	glUniformMatrix4fv(iMVP, 1, false, glm::value_ptr(MVP));


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDrawElements(GL_TRIANGLES, 36*block_i, GL_UNSIGNED_INT, 0);
//	glDrawArrays(GL_POINTS, 0, theta_divisions*phi_divisions);

}

glm::mat4 makeMVP(GLFWwindow* window){
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glm::mat4 P = glm::perspectiveFov(120.0f, (float) width, (float) height, 0.01f, 1000.0f);
	glViewport(0, 0, (float) width, (float) height);
	glm::mat4 MV = view_matrix(camera);
	return P * MV;
}
void after_render_loop(){
	glDeleteVertexArrays(1, &vertex_array_object);
	glDeleteVertexArrays(1, &vertex_array_object);
}

void cursorpos_callback(GLFWwindow *window, double xpos, double ypos){
	float angle_inc = 0.0005;
	look_up(camera, -angle_inc*ypos);
	look_left(camera, -angle_inc*xpos);
	glfwSetCursorPos(window, 0, 0);
}

void key_update(char* key_array){
	float angle_inc = 0.05;
	float distance_inc = 0.8;
	if (key_array[GLFW_KEY_UP]){			look_up(camera, angle_inc); }
	if (key_array[GLFW_KEY_LEFT]){			look_left(camera, angle_inc); }
	if (key_array[GLFW_KEY_DOWN]){			look_up(camera, -angle_inc); }
	if (key_array[GLFW_KEY_RIGHT]){			look_left(camera, -angle_inc); }
	if (key_array[GLFW_KEY_W]){			move_forward(camera, distance_inc); }
	if (key_array[GLFW_KEY_S]){			move_forward(camera, -distance_inc); }
	if (key_array[GLFW_KEY_A]){			move_left(camera, distance_inc); }
	if (key_array[GLFW_KEY_D]){			move_left(camera, -distance_inc); }
	if (key_array[GLFW_KEY_SPACE]){			move_up(camera, distance_inc); }
	if (key_array[GLFW_KEY_LEFT_SHIFT]){		move_up(camera, -distance_inc); }
	if (key_array[GLFW_KEY_1]){			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);}
	if (key_array[GLFW_KEY_2]){			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);}
	if (key_array[GLFW_KEY_3]){			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);}
}

int main(){
	struct Application my_app;
	my_app.vertex_source_path   	= "v.shader";
	my_app.fragment_source_path 	= "f.shader";
	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_update		= &key_update;
	my_app.cursorpos_callback	= &cursorpos_callback;

	printf("GLFW_KEY_LAST - %d", GLFW_KEY_LAST);
	run(my_app);
}
