#include <stdlib.h>
#include <stdio.h>
#define checknode(nodem) if (nodem > 100){ \
			printf("NO! FAIL!\n"); \
			exit(1); \
		}

typedef struct node_link_ts{
	short node_index;
	/* Distance? */
	struct node_link_ts* link;
} node_link_t;

typedef struct node_ts{
	float x, y;
	node_link_t* neighbours;
} node_t;

typedef struct path_link_ts{
	short node_index;
	node_link_t* neighbour_link;
	struct path_link_ts* link;
	struct path_link_ts* backward_link;
} path_link_t;

void add_to_neighbours(node_t* nodes, short node, short node_neighbour){
	node_link_t* new_node_link = (node_link_t*) malloc(sizeof(node_link_t));
	new_node_link->node_index = node_neighbour;
	new_node_link->link = nodes[node].neighbours;
	nodes[node].neighbours = new_node_link;
}
void add_node_to_path(path_link_t* path_link, short node, node_t* nodes){
	path_link_t* new_path_link = (path_link_t*) malloc(sizeof(path_link_t));
	new_path_link->node_index = node;
	new_path_link->neighbour_link = nodes[node].neighbours;
	new_path_link->link = NULL;
	new_path_link->backward_link = path_link;
	path_link->link = new_path_link;
}

void add_to_node_list(node_link_t* node_list, short node){
	node_link_t* new_node_link = (node_link_t*) malloc(sizeof(node_link_t));
	new_node_link->node_index = node;
	new_node_link->link = node_list->link;
	node_list->link = new_node_link;
}

//if (is_inside_list(visited_nodes, neighbour->node_index)){
char is_inside_list(node_link_t* node_list, short node){
	checknode(node);
	for (node_link_t* node_link = node_list;
	     node_link != NULL;
	     node_link = node_link->link){
		if (node_link->node_index == node){
			return 1;
		}
	}
	return 0;
}

void print_path(path_link_t* path_head){
	printf("Path - [");
	path_link_t* path_link = path_head;
	while(path_link != NULL){
		path_link_t* next_link = path_link->link;
		printf(", %d", path_link->node_index);
		path_link=next_link;
	}
	printf("]\n");
	char x[50];
	//scanf("%s", x);
}

void print_node_list(node_link_t* node_list_head){
	printf("Node List - [");
	node_link_t* node_link = node_list_head;
	while(node_link != NULL){
		node_link_t* next_link = node_link->link;
		printf(", %d", node_link->node_index);
		node_link=next_link;
	}
	printf("]\n");
	char x[50];
	//scanf("%s", x);
}

void free_path(path_link_t* path_head){
	path_link_t* path_link = path_head;
	while(path_link != NULL){
		path_link_t* next_link = path_link->link;
		free(path_link);
		path_link=next_link;
	}
}

path_link_t* dfs(node_t* nodes, short start, short goal){
	path_link_t* path_head = (path_link_t*) malloc(sizeof(path_link_t));
	path_head->node_index = start;
	path_head->neighbour_link = nodes[start].neighbours;
	printf("start's node list: ");
	print_node_list(nodes[start].neighbours);
	path_head->link = NULL;
	path_head->backward_link = NULL;

	path_link_t* path_tail = path_head;

	//to stop it from going around in a circle
	node_link_t* visited_nodes = (node_link_t*) malloc(sizeof(node_link_t));
	visited_nodes->node_index = start;
	visited_nodes->link = NULL;

	short current_node = start;
	goto for_the_start_node;

	while_loop: //to break out multiple loops
	while (1){
		checknode(current_node);
		print_path(path_head);
		print_node_list(visited_nodes);
		add_to_node_list(visited_nodes, current_node);
		add_node_to_path(path_tail, current_node, nodes);
		path_tail = path_tail->link;

		for_the_start_node:
		if (current_node == goal){
			//add_node_to_path(path_tail, goal, nodes);
			return path_head;
		}
		for (node_link_t* neighbour = path_tail->neighbour_link;
		     neighbour != NULL;
		     neighbour = neighbour->link){
			printf("%d's node list: ", neighbour->node_index);
			print_node_list(nodes[neighbour->node_index].neighbours);
			if (is_inside_list(visited_nodes, neighbour->node_index)){
				printf("REPEAT!!! I DECLARE!!!");
				char x[50];
				//scanf("%s", x);
				printf("As expected maybe.... %d = %d\n", neighbour->node_index, current_node);
				continue; //to next iteration in for loop
			}
			//change this to current_node and traverse this
			path_tail->neighbour_link = neighbour;
			current_node = neighbour->node_index;
			goto while_loop;
		}
		//all the neighbours of current_node have been traversed already
		//go back to the last node in path with a non-null neighbour_link
		//i.e a node with some neighbours still left unchecked
		for (path_link_t* path_link = path_tail->backward_link;
		     path_link != NULL;
		     path_link = path_link->backward_link){
			free(path_link->link);
			if (path_link->neighbour_link != NULL){
				path_tail = path_link;
				current_node = path_link->node_index;
				goto while_loop;
			}
		}
		//no path possible
		return NULL;
	}
	return path_head;
}

