#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.h"
#include "nodes.c"

//test

//CONFIG
GLfloat bgcolor[] = {0.0f, 0.0f, 0.0f, 1.0f};
float point_radius = 5;
float point_deviation_x = 15*point_radius / 1920;
float point_deviation_y = 15*point_radius / 1080;
unsigned int pmax = 100; //must be less than 2^16 = 65k, or else change short to int for data type of line_ibo
unsigned int lmax = 100;

//header file?
enum point_manipulation_enum { //with left click
	ADD_POINT,
	DRAGGING_POINT,
	DELETE_POINT,
};

enum line_manipulation_enum { //with right click
	START_ADD_LINE,
	END_ADD_LINE,
	DELETE_LINE_START,
	DELETE_LINE_END,
};

//app var's
node_t* nodes;
unsigned int pnum = 1; //mouse is index 0
unsigned int lnum = 0;
int point_info = ADD_POINT;
int line_info = START_ADD_LINE;
short drag_index = -1;

//opengl var's
GLuint vao 		= -1;
GLuint vbo 		= -1;
GLuint line_ibo 	= -1;
GLuint program		= -1;

void before_render_loop(struct Application* app){
	nodes = (node_t*) malloc(pmax * sizeof(node_t));

	program = app->program;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, pmax*2*sizeof(float), NULL, GL_DYNAMIC_DRAW);

	glGenBuffers(1, &line_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, line_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, lmax*2*sizeof(short), NULL, GL_DYNAMIC_DRAW);
}

void render(struct Application* app){
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);

	glPointSize(2*point_radius);
	glDrawArrays(GL_POINTS, 1, pnum-1);
	glDrawElements(GL_LINES, 2*lnum, GL_UNSIGNED_SHORT, 0);
}

short get_node_under_cursor(float xpos, float ypos){
	for (short i = 1; i < pmax; i++){
		if ((fabs(xpos - nodes[i].x) <= point_deviation_x) &&
		    (fabs(ypos - nodes[i].y) <= point_deviation_y)) {
			return i;
		}
	}
	return -1;
}

void* map_buff(GLenum buffer_enum, size_t location, size_t length){
	void* point_buffer = glMapBufferRange(	buffer_enum,
						location,
						length,
						GL_MAP_WRITE_BIT);
	if (point_buffer == NULL) {
		printf("glMapBufferRange Failed, null pointer.\n");
		exit(1);
	}
	return point_buffer;
}

void create_new_node(float x, float y){
	if (pnum >= pmax){
		printf("Max no. of points (%d) has been reached.\nUnable to draw more points.\n", pmax);
		return;
	}

	float* pb = (float*) map_buff(GL_ARRAY_BUFFER, pnum*2*sizeof(float), 2*sizeof(float));
	pb[0] = x;
	pb[1] = y;
	glUnmapBuffer(GL_ARRAY_BUFFER);

	nodes[pnum].x = x;
	nodes[pnum].y = y;
	pnum++;
}

void drop_dragged_point_here(float xpos, float ypos){
	if (drag_index <= 0) {
		printf("Fatal dragging error.\n");
		exit(1);
	}

	float* pb = (float*) map_buff(GL_ARRAY_BUFFER, drag_index*2*sizeof(float), 2*sizeof(float));
	pb[0] = xpos;
	pb[1] = ypos;
	glUnmapBuffer(GL_ARRAY_BUFFER);
	nodes[drag_index].x = xpos;
	nodes[drag_index].y = ypos;
	drag_index = -1;
	glUniform1i(glGetUniformLocation(program, "drag_index"), drag_index);
	point_info = ADD_POINT;
}

void drag_point(short node){
	if (drag_index >= 0) {
		printf("Fatal dragging error.\n");
		exit(1);
	}
	drag_index = node;
	nodes[drag_index].x = -500; //throwing it off into the distance so no ghost behaviour
	nodes[drag_index].y = -500;
	glUniform1i(glGetUniformLocation(program, "drag_index"), drag_index);
	point_info = DRAGGING_POINT;
}

void start_new_line(short node){
	if (lnum >= lmax){
		printf("Max no. of lines (%d) has been reached.\nUnable to draw more lines.\n", lmax);
		return;
	}

	short* pb = (short*) map_buff(GL_ELEMENT_ARRAY_BUFFER, lnum*2*sizeof(short), 2*sizeof(short));
	pb[0] = node;
	pb[1] = 0;
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	lnum++;
	line_info = END_ADD_LINE;
}

void end_new_line(short node){
	short* pb = (short*) map_buff(GL_ELEMENT_ARRAY_BUFFER, (lnum*2 - 1)*sizeof(short), 1*sizeof(short));
	pb[0] = node;
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	line_info = START_ADD_LINE;
}

void get_cursor_normalized(GLFWwindow* window, float* xp, float* yp){
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	printf("button pressed at (%f, %f) ", xpos, ypos);

	*xp = xpos;
	*yp = ypos;
	*xp = 2*(*xp)/1920 - 1;
	*yp = 1 - 2*(*yp)/1080;
	printf("or (%f, %f).\n", *xp, *yp);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
	printf("drag index is %d\n", drag_index);
	float x,y;
	get_cursor_normalized(window, &x, &y);
	short clicked_node = get_node_under_cursor(x, y);
	switch(action){
	case GLFW_PRESS:
		switch(button){
		case GLFW_MOUSE_BUTTON_LEFT:
			if (clicked_node > 0) {
				switch(point_info){
				case ADD_POINT:
					drag_point(clicked_node);
					break;
				case DRAGGING_POINT://switch them
					drop_dragged_point_here(x, y);
					drag_point(clicked_node);
					break;
				}
			} else {
				switch(point_info){
				case ADD_POINT:
					create_new_node(x, y);
					break;
				case DRAGGING_POINT:
					drop_dragged_point_here(x, y);
					break;
				}
			}
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			if (clicked_node > 0) {
				switch(line_info){
				case START_ADD_LINE:
					start_new_line(clicked_node);
					break;
				case END_ADD_LINE:
					end_new_line(clicked_node);
					break;
				}
			}
			//do nothing if no node clicked
			break;
		}
	}
}


void cursor_position_callback(GLFWwindow *window, double xpos, double ypos){
	float x = 2*xpos/1920 - 1;
	float y = 1 - 2*ypos/1080;
	glUniform2f(glGetUniformLocation(program, "mouse_position"), x, y);
}


void key_update(char* key_array){
}

void after_render_loop(){
}

int main(){
	struct Application my_app;
	default_app(&my_app);
	my_app.shader_types[0] 		= GL_VERTEX_SHADER;
	my_app.shader_types[1] 		= GL_FRAGMENT_SHADER;
	my_app.shader_source_paths[0] 	= "vertex.shader";   //pass through
	my_app.shader_source_paths[1] 	= "fragment.shader"; //pass through

	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_update		= &key_update;
	my_app.cursor_position_callback	= &cursor_position_callback;
	my_app.mouse_button_callback	= &mouse_button_callback;

	run(&my_app);
}
