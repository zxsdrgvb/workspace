#version 330

in vec4 vcolor;
out vec4 color;

vec4 red 	= vec4(1, 0, 0, 1);
vec4 blue 	= vec4(0, 0, 1, 1);
void main(){
	color = vcolor;
}
