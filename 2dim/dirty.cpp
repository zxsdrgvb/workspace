#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.h"
#include "nodes.c"

GLfloat bgcolor[] = {0.0f, 0.0f, 0.0f, 1.0f};
float point_radius = 5;
float point_deviation_x = 15*point_radius / 1920;
float point_deviation_y = 15*point_radius / 1080;

typedef struct {short node1, node2;} line_t;

unsigned int pmax = 100; //must be less than 2^16 = 65k, or else change short to int for data type of line_ibo
unsigned int pnum = 1; //mouse always counts as 1
unsigned int lmax = 100;
unsigned int lnum = 0;
node_t* nodes;
line_t* lines;

enum App_information_enum {
	ADD_POINT 			= 1 << 0,
	ADD_START_LINE   		= 1 << 1,
	ADD_END_LINE 	  		= 1 << 2,
	DELETE_POINT			= 1 << 3,
	DELETE_START_LINE   		= 1 << 4,
	DELETE_END_LINE   		= 1 << 5,
	DRAGGING_POINT			= 1 << 6,

};

int app_info = ADD_POINT;
short drag_index = -1;

GLuint vao 		= -1;
GLuint vbo 		= -1;
GLuint line_ibo 	= -1;
GLuint program		= -1;
void before_render_loop(struct Application* app){
	program = app->program;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	//glBufferStorage(GL_ARRAY_BUFFER, pmax*2*sizeof(float), NULL, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
	glBufferData(GL_ARRAY_BUFFER, pmax*2*sizeof(float), NULL, GL_DYNAMIC_DRAW);
	//if memory is that much of an issue, change to short and cast later.
	//float* point_buffer = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	//glUnmapBuffer(GL_ARRAY_BUFFER);

	glGenBuffers(1, &line_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, line_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, lmax*2*sizeof(short), NULL, GL_DYNAMIC_DRAW);

	nodes = (node_t*) malloc(pmax * sizeof(node_t));
}

void render(struct Application* app){
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);

	glPointSize(2*point_radius);
	glDrawArrays(GL_POINTS, 0, pnum);
	glDrawElements(GL_LINES, 2*lnum, GL_UNSIGNED_SHORT, 0);
}

void after_render_loop(){
}

void cursor_position_callback(GLFWwindow *window, double xpos, double ypos){
	float x = xpos/1920;
	float y = ypos/1080;
	x = 2*x - 1;
	y = 1 - 2*y;
	glUniform2f(glGetUniformLocation(program, "mouse_position"), x, y);
	//printf("Cursor Moves! to (%f, %f)\n", x, y);
}

short get_node_under_cursor(float xpos, float ypos){
	for (short i = 1; i < pmax; i++){
		if ((fabs(xpos - nodes[i].x) <= point_deviation_x) &&
		    (fabs(ypos - nodes[i].y) <= point_deviation_y)) {
			return i;
		}
	}
	return -1;
}



void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
	if (action == GLFW_PRESS){
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		printf("button pressed at (%f, %f) ", xpos, ypos);
		xpos = 2*xpos/1920 - 1;
		ypos = 1 - 2*ypos/1080;
		printf("or (%f, %f).\n", xpos, ypos);
		short clicked_node = get_node_under_cursor(xpos, ypos);
		if (button == GLFW_MOUSE_BUTTON_LEFT){
			if (clicked_node <= -1){
				if (app_info & ADD_POINT){
					//create new node
					if (pnum >= pmax){
						printf("Max no. of points (%d) has been reached.\nUnable to draw more points.\n", pmax);
						return;
					}
					float* point_buffer = (float*) //TODO: make this into a function
						glMapBufferRange(GL_ARRAY_BUFFER,
								 pnum*2*sizeof(float),
								 2*sizeof(float),
								 GL_MAP_WRITE_BIT);
					if (point_buffer == NULL) {
						printf("Null map pointer\n");
						exit(1);
					}
					point_buffer[0] = (float) xpos;
					point_buffer[1] = (float) ypos;

					glUnmapBuffer(GL_ARRAY_BUFFER);

					nodes[pnum].x = xpos;
					nodes[pnum].y = ypos;
					pnum++;
					return;
				} else if (app_info & DRAGGING_POINT){
					//move old node
					float* point_buffer = (float*)
						glMapBufferRange(GL_ARRAY_BUFFER,
								 drag_index*2*sizeof(float),
								 2*sizeof(float),
								 GL_MAP_WRITE_BIT);
					point_buffer[0] = (float) xpos;
					point_buffer[1] = (float) ypos;
					glUnmapBuffer(GL_ARRAY_BUFFER);
					drag_index = -1;
					glUniform1i(glGetUniformLocation(program, "drag_index"), drag_index);
					app_info ^= DRAGGING_POINT;
					app_info |= ADD_POINT;
					return;
				}
			} else {
				//drag this node
				drag_index = clicked_node;
				glUniform1i(glGetUniformLocation(program, "drag_index"), drag_index);
				app_info |= DRAGGING_POINT;
				app_info ^= ADD_POINT;
			}
		} else if ((button == GLFW_MOUSE_BUTTON_RIGHT)){
			if (clicked_node <= -1) {
				printf("No node under cursor.\n");
				return;
			} else {
				printf("Node found : %d.\n", clicked_node);
			}
			if (lnum >= lmax){
				printf("Max no. of lines (%d) has been reached.\nUnable to draw more lines.\n", lmax);
				return;
			}

			if (app_info & ADD_END_LINE) {
				short* line_buffer = (short*)
					glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER,
							 (lnum*2 - 1)*sizeof(short),
							 sizeof(short),
							 GL_MAP_WRITE_BIT);
				if (line_buffer == NULL) {
					printf("Null map pointer\n");
					exit(1);
				}
				line_buffer[0] = clicked_node;
				glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
				app_info ^= ADD_END_LINE;
			} else {
				short* line_buffer = (short*)
					glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER,
							 lnum*2*sizeof(short),
							 2*sizeof(short),
							 GL_MAP_WRITE_BIT);
				if (line_buffer == NULL) {
					printf("Null map pointer\n");
					exit(1);
				}
				line_buffer[0] = clicked_node;
				line_buffer[1] = 0;

				glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
				app_info |= ADD_END_LINE;
				lnum++;
			}
		}
	}
}

void key_update(char* key_array){
}

int main(){
	struct Application my_app;
	default_app(&my_app);
	my_app.shader_types[0] 		= GL_VERTEX_SHADER;
	my_app.shader_types[1] 		= GL_FRAGMENT_SHADER;
	my_app.shader_source_paths[0] 	= "vertex.shader";   //pass through
	my_app.shader_source_paths[1] 	= "fragment.shader"; //pass through

	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_update		= &key_update;
	my_app.cursor_position_callback	= &cursor_position_callback;
	my_app.mouse_button_callback	= &mouse_button_callback;

	run(&my_app);
}
