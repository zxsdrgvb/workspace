#version 330

in vec2 point;
uniform vec2 mouse_position;
uniform int drag_index;
out vec4 vcolor;

vec4 red  = vec4(1, 0, 0, 1);
vec4 blue = vec4(0, 0, 1, 1);
vec4 yellow = vec4(1, 1, 0, 1);
void main(){
	if (gl_VertexID == drag_index){
		gl_Position = vec4(mouse_position.x, mouse_position.y, 0, 1);
		vcolor = red;
		return;
	}
	switch(gl_VertexID){
	case 0:
		gl_Position = vec4(mouse_position.x, mouse_position.y, 0, 1);
		gl_PointSize = 1;
		vcolor = blue;
		return;
	case 1:
		gl_Position = vec4(point.x, point.y, 0, 1);
		vcolor = yellow;
		return;
	default:
		gl_Position = vec4(point.x, point.y, 0, 1);
		vcolor = vec4(1,1,1,1);
		return;
	}
}
