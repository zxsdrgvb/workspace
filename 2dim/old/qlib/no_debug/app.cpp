#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <signal.h>
#include "app.h"

int run(struct Application* app){
	init_window(app);
	glewInit();

	GLFWwindow* window = app->window;
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, app->cursorpos_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(window, 0, 0);

	printf("Current GL version: %s\n", glGetString(GL_VERSION));
	printf("Current GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	compile_shaders(app);
	(*app->before_render_loop)();
	while (!glfwWindowShouldClose(window)){
		glUseProgram(app->program);
		(*app->key_update)(app->key_array);
		(*app->render)(app);
		glfwSwapBuffers(app->window);
		glfwPollEvents();
	}
	(*app->after_render_loop)();

	glDeleteProgram(app->program);
	glfwDestroyWindow(app->window);
	glfwTerminate();
	return 0;
}

void init_window(struct Application *app){
	if (!glfwInit()){
		fprintf(stderr, "GLFW failed to initialize.");
		exit(1);
	}
	//glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	//glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );

	app->window = glfwCreateWindow(1920, 1080, "Event Tester", NULL, NULL);
	if (!(app->window)){
		fprintf(stderr, "GLFW failed to create window.\n");
		exit(1);
	}
	glfwMakeContextCurrent(app->window);
	glfwSetWindowUserPointer(app->window, app);
	for (int i = 0; i < GLFW_KEY_LAST + 1; i++){
		app->key_array[i] = 0;
	}
}

void compile_shaders(struct Application* app){
	app->program = glCreateProgram();
	if (app->program <= 0) exit(1);
	for (int shader_index = 0; shader_index < NO_OF_SHADERS; shader_index++){
		compile_single_shader(app, shader_index);
		glAttachShader(app->program, app->shader[shader_index]);
	}
	glLinkProgram(app->program);
}

void compile_single_shader(struct Application* app, int shader_index){
	const char* source_path = app->shader_source_paths[shader_index];
	GLenum shader_type 	= app->shader_types[shader_index];
	FILE* fd = fopen(source_path, "r");
	if (!fd){
		fprintf(stderr, "ERROR: %s failed to open. Doesn't exist?\n", source_path);
		exit(1);
	}

	//getting file size
	fseek(fd, 0L, SEEK_END);
	long filesize = ftell(fd); //in no. of bytes, so no. of chars
	fseek(fd, 0L, SEEK_SET);
	//printf("\nSize of %s is %d bytes.\n", source_path, filesize);

	//mallocing buffer
	char* buffer = (char*) malloc(filesize + 1);
	if (!buffer){
		fprintf(stderr, "Buffer malloc for %d bytes failed.\n", filesize);
		exit(1);
	}
	//reading into buffer and null terminating
	size_t bytes_read = fread(buffer, sizeof(char), filesize, fd);
	if (bytes_read != filesize){
		fprintf(stderr, "%d bytes read, not equal to filesize: %d bytes.\n", bytes_read, filesize);
		exit(1);
	}
	buffer[filesize] = '\0'; //OpenGL requires this or else I'll have to specify filesize
	app->shader_sources[shader_index] = buffer;

	//compiling shader
	GLint shader = glCreateShader(shader_type);
	if (shader <= 0) {
		fprintf(stderr, "Shader creation failed.\n");
		exit(1);
	}
	glShaderSource(shader, 1, &(app->shader_sources[shader_index]), NULL);
	glCompileShader(shader);
	app->shader[shader_index] = shader;

	//printing buffer to terminal
	printf("----------%s (%d bytes)----------\n%s----------END OF %s----------\n",
			source_path,
			bytes_read,
			buffer,
			source_path);

	//printing debug info
	GLint log_length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length == 0) printf("COMPILATION LOG FOR %s EMPTY\n", source_path);
	else {
		char* log_buffer = (char*) malloc(log_length);
		if (!log_buffer) {
			fprintf(stderr, "Log buffer malloc failed for %d bytes.\n", log_length);
			exit(1);
		}
		glGetShaderInfoLog(shader, log_length, NULL, log_buffer);
		printf("COMPILATION LOG FOR %s:\n%s\n", source_path, log_buffer);
		free(log_buffer);
	}

	//printing compilation status
	GLint is_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
	if (is_compiled){
		printf("Compilation of %s successful.\n\n", source_path);
	} else {
		fprintf(stderr, "COMPILATION OF %s FAILED.\n\n", source_path);
		exit(1);
	}
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS)){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
		return;
	}
	struct Application* app = (struct Application*) glfwGetWindowUserPointer(window);
	if (key == GLFW_KEY_UNKNOWN){return;} //GLFW_KEY_UNKNOWN is -1
	if (action == GLFW_PRESS) {	app->key_array[key] = (char) true;}
	if (action == GLFW_RELEASE){ 	app->key_array[key] = (char) false;}
}
