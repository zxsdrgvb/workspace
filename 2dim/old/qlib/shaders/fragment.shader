#version 330 core

in float vertex_id;
in float pos_z;
//in vec3 position
out vec4 color;
void main() {
	vec4 white = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	vec4 grey = vec4(.5f, .5f, .5f, 1.0f);
	vec4 black = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	vec4 yellow = vec4(0.6f, 0.6f, 0.0f, 1.0f);
	vec4 pink = vec4(199, 21, 133, 255);
//	color = white;
//	if (pos_z < 0) color = pink/255;
//	else color = grey;
//	color = vec4((sin(vertex_id*0.5 + 0.5))*vec3(1.0f, 1.0f, 1.0f), 1.0f);
//	color = 0.5*vec4(sin(vertex_id), cos(vertex_id), 1/(vertex_id), 1.0f) + 0.5;
//	color = vec4((0.5*(sin(2*3.141519267*vertex_id/459))+0.5)*vec3(1.0f, 1.0f, 1.0f), 1.0f);
//	color = yellow;
	color = vec4(sin(gl_FragCoord.x/10), cos(gl_FragCoord.y/10), 0.5, 1.0)*0.5 + 0.5;
}
