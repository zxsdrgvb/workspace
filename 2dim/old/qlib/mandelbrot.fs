#version 400 core

out vec4 color;

vec4 white = vec4(1, 1, 1, 1);
vec4 black = vec4(0, 0, 0, 1);

in vec4 pos_fs;
//uniform vec4 boundary;
vec4 boundary = vec4(-1.92, -1.08, 1.92, 1.08);
//uniform uint max;
uint max = 20;
//uniform float zx0;
//uniform float zy0;

float scale = 2;
void main(){
	float cx = pos_fs.x/2 - pos_fs.z/3 ;
//	float cy = pos_fs.y/2 - pos_fs.z/3 ;
	float cy = pos_fs.z/2 - pos_fs.y/3;
//	float cx = pos_fs.x;
//	float cy = pos_fs.y;

	float zx = 0;
	float zy = 0;
	uint i = 0;
	while (((zx*zx + zy*zy) < 4) && (i < max)){
		float zx_n = zx*zx -2*zx*zy + cx;
		zy = zy*zy + cy;
		zx = zx_n;
		i++;
	}
	vec3 max_color;
	if (pos_fs.z > 0) max_color = vec3(1,0,1);
	else max_color = vec3(0, 1, 0);
	color = vec4(float(i) / float(max) * max_color, 1);
}
