#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/detail/type_vec2.hpp>

#include <stdio.h>
#include <cmath>
#include "lib/app.h"

#define NO_OF_POINTS 6
GLfloat bgcolor[] = {0.0f, 0.0f, 0.0f, 1.0f};

GLuint vao = -1;
GLuint vbo = -1;
void before_render_loop(){
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, NO_OF_POINTS*2*sizeof(float), NULL, GL_STATIC_DRAW);

	float* point_buffer = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	point_buffer[0] =  0.5f;
	point_buffer[1] = -0.5f;
	point_buffer[2] = -0.2f;
	point_buffer[3] =  0.1f;
	point_buffer[4] =  0.6f;
	point_buffer[5] =  0.3f;
	glUnmapBuffer(GL_ARRAY_BUFFER);
}

void render(struct Application* app){
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glClearBufferfv(GL_COLOR, 0, bgcolor);
	glPointSize(10.0f);
	glDrawArrays(GL_POINTS, 0, NO_OF_POINTS);
}

void after_render_loop(){
}

void cursor_position_callback(GLFWwindow *window, double xpos, double ypos){

}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		printf("Left button pressed at (%f, %f).\n", xpos, ypos);
	}
}

void key_update(char* key_array){
//	printf("Key is active.\n");
}

int main(){
	struct Application my_app;
	my_app.shader_types[0] 		= GL_VERTEX_SHADER;
	my_app.shader_types[1] 		= GL_FRAGMENT_SHADER;
	my_app.shader_source_paths[0] 	= "vertex.shader";   //pass through
	my_app.shader_source_paths[1] 	= "fragment.shader"; //pass through

	my_app.before_render_loop   	= &before_render_loop;
	my_app.render			= &render;
	my_app.after_render_loop	= &after_render_loop;
	my_app.key_update		= &key_update;
	my_app.cursor_position_callback	= &cursor_position_callback;
	my_app.mouse_button_callback	= &mouse_button_callback;

	run(&my_app);
}
