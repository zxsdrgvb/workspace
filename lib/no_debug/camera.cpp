#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <stdio.h>

glm::vec3 Up = {0.0f, 1.0f, 0.0f};
struct Camera{
	glm::vec3 eye; //position
	glm::vec3 forward;
};

glm::mat4x4 view_matrix(Camera const& cam){
//	return glm::translate(cam.eye) * glm::orientation(cam.forward, cam.up);
	return glm::lookAt(cam.eye, cam.eye + cam.forward, Up);
}

glm::vec3 right_normal(Camera &cam){
	return glm::cross(cam.forward, Up);
}

void look_left(Camera &cam, float const& angle){
	cam.forward = glm::rotate(cam.forward, angle, Up);
}

void look_up(Camera &cam, float const& angle){
	cam.forward = glm::rotate(cam.forward, angle, right_normal(cam));
}

void translate(Camera &cam, glm::vec3 const& translation){
	cam.eye += translation;
}
void move_forward(Camera &cam, float const& distance){
	cam.eye += distance * cam.forward;
}

void move_up(Camera &cam, float const& distance){
	cam.eye += distance * Up;
}

void move_left(Camera &cam, float const& distance){
	cam.eye += -distance * right_normal(cam);
}

// //Ah I'll get gimbal locked...
